import simulation.SimulationFrame;
import simulation.SimulationManager;

public class Main {
    public static void main(String[] args){
        SimulationFrame frame = new SimulationFrame();
        frame.setVisible(true);
        SimulationManager sim = new SimulationManager(frame);
    }
}
