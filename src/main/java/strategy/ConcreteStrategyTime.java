package strategy;

import server.Server;
import server.Task;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addTask(ArrayList<Server> servers, Task t) {
        int minTime = 1000000000;
        for(Server s: servers){
            if(s.getWaitingPeriod().intValue() < minTime){
                minTime = s.getWaitingPeriod().intValue();
            }
        }
        for(Server s: servers){
            if(s.getWaitingPeriod().intValue() == minTime){
                s.addTask(t);
                break;
            }
        }
    }
}
