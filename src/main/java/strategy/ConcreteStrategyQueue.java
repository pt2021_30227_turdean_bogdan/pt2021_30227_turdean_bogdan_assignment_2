package strategy;

import server.Server;
import server.Task;

import java.util.ArrayList;

public class ConcreteStrategyQueue implements Strategy {
    @Override
    public void addTask(ArrayList<Server> servers, Task t) {
        int minSize = servers.get(0).getTasks().size();
        for(Server s: servers){
            if(s.getTasks().size() < minSize)
                minSize = s.getTasks().size();
        }
        for(Server s: servers){
            if(s.getTasks().size()==minSize)
            {
                s.addTask(t);
                break;

            }
        }
    }
}
