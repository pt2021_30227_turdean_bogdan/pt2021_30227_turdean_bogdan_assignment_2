package simulation;

import server.Server;
import server.Task;
import strategy.Scheduler;
import strategy.SelectionPolicy;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class SimulationManager implements Runnable{
    private final int timeLimit;
    private final int maxProcessingTime;
    private final int minProcessingTime;
    private final int numberOfServers;
    private final int numberOfClients;
    private final int maxArrivalTime;
    private final int minArrivalTime;
    private final SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;

    private final Scheduler scheduler;

    private final SimulationFrame frame;

    private ArrayList<Task> generatedTasks;

    public SimulationManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int numberOfClients, int numberOfServers, int minArrivalTime, int maxArrivalTime, SimulationFrame frame) {
        this.frame = frame;

        this.timeLimit = timeLimit;
        this.maxArrivalTime = maxArrivalTime;
        this.minArrivalTime = minArrivalTime;
        this.maxProcessingTime = maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.numberOfClients = numberOfClients;
        this.numberOfServers = numberOfServers;

        this.scheduler = new Scheduler(numberOfServers, 10);
        this.scheduler.changeStrategy(selectionPolicy);
        for (Server s : scheduler.getServers()) {
            Thread thread = new Thread(s);
            thread.start();
        }
        generateRandomTasks();
        frame.startActionListener(new StartListener());
        frame.resetActionListener(new ResetListener());
        frame.mediiActionListener(new MediiListener());
    }

    public SimulationManager(SimulationFrame frame){
        this.frame = frame;
        this.timeLimit = 0;
        this.maxArrivalTime = 0;
        this.minArrivalTime = 0;
        this.maxProcessingTime = 0;
        this.minProcessingTime = 0;
        this.numberOfClients = 0;
        this.numberOfServers = 0;

        this.scheduler = new Scheduler(numberOfServers, 10);
        this.scheduler.changeStrategy(selectionPolicy);
        frame.startActionListener(new StartListener());
        frame.resetActionListener(new ResetListener());
        frame.mediiActionListener(new MediiListener());
    }

    private void generateRandomTasks(){
        Random rand = new Random();
        this.generatedTasks = new ArrayList<>();
        for(int i=0; i<numberOfClients; i++){
            int processingTime = rand.nextInt(maxProcessingTime - minProcessingTime + 1) + minProcessingTime;
            int arrivalTime = rand.nextInt(maxArrivalTime - minArrivalTime + 1) + minArrivalTime;
            Task newTask = new Task(i+1, arrivalTime, processingTime);
            this.generatedTasks.add(newTask);

        }
        Collections.sort(generatedTasks);
    }

    private void printServers(){
        int i = 1;
        for(Server s: scheduler.getServers()){
            System.out.println("Queue "  + i + ": " + s.toString());
            i++;
        }

    }

    private void printServersTxt(FileWriter f) throws IOException {
        int i = 1;
        for(Server s: scheduler.getServers()){
            f.write("Queue " + i + ": " + s.toString() + "\n");
            i++;
        }
        f.write("\n");
    }

    private void printServersFrame(SimulationFrame frame){
        int i = 1;
        for(Server s: scheduler.getServers()){
            frame.setArea("Queue " + i + ": " + s.toString() + "\n");
            i++;
        }
        frame.setArea("\n");
    }

    private void printQueue(ArrayList<Task> list){
        System.out.println("Waiting clients:");
        for(Task t: list){
            System.out.println(t.toString() + " ; ");
        }
    }

    private void printQueueTxt(ArrayList<Task> list, FileWriter f) throws IOException {
        f.write("Waiting clients: \n");
        for(Task t: list){
            f.write(t.toString() + " ; ");
        }
        f.write("\n");
    }

    private void printQueueFrame(ArrayList<Task> list, SimulationFrame frame){
        frame.setArea("Waiting clients: \n");
        for(Task t: list){
            frame.setArea(t.toString() + " ; ");
        }
        frame.setArea("\n");
    }

    @Override
    public synchronized void run() {
        int currentTime = 0;
        try {
            FileWriter f = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_2\\src\\main\\java\\out.txt");
            while (currentTime <= timeLimit) {
                //timpul curent
                String s = "Time = " + currentTime;
                System.out.println(s);
                f.write("Time: " + currentTime + "\n");
                frame.setArea("Time: " + currentTime + "\n");

                Collections.sort(this.generatedTasks);
                ArrayList<Task> list = new ArrayList<>();
                for (Task t : this.generatedTasks) {
                    if (t.getArrivalTime() == currentTime)
                        list.add(t);
                }
                for (Task t : list) {
                    this.scheduler.dispatchTask(t);
                    this.generatedTasks.remove(t);
                }
                printQueue(this.generatedTasks); //waiting list
                printQueueTxt(this.generatedTasks, f);
                printQueueFrame(this.generatedTasks, frame);
                printServers();//cozile
                printServersTxt(f);
                printServersFrame(frame);
                System.out.println();

                //actualizare processing time
                for (Server serv : scheduler.getServers()) {
                    Task t = null;
                    for (Iterator<Task> i = serv.getTasks().iterator(); i.hasNext(); ) {
                        t = i.next();
                        break;
                    }
                    if (t != null) {
                        serv.getTasks().element().setProcessingPeriod(serv.getTasks().element().getProcessingPeriod() - 1);
                        if (serv.getTasks().element().getProcessingPeriod() == 0)
                            serv.getTasks().remove(serv.getTasks().element());
                        serv.setWaitingPeriod(new AtomicInteger(serv.getWaitingPeriod().intValue() - 1));
                    }
                }


                currentTime++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            f.close();
        }
        catch (IOException e) {
                e.printStackTrace();
            }

    }

    class StartListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            frame.initArea();
            try {
                int numberOfClienti1 = frame.getNoClienti();
                int numberOfServers1 = frame.getNoServers();
                int timeLimit1 = frame.getTimeLimit();
                int minProcessing1 = frame.getMinProcessing();
                int maxProcessing1 = frame.getMaxProcessing();
                int minArrival1 = frame.getMinArrival();
                int maxArrival1 = frame.getMaxArrival();

                SimulationManager sim = new SimulationManager(timeLimit1, maxProcessing1, minProcessing1, numberOfClienti1, numberOfServers1, minArrival1, maxArrival1, frame);
                Thread t = new Thread(sim);
                t.start();
            }
            catch (NumberFormatException f){
                JOptionPane.showMessageDialog(null, "Introduceti numere", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    class ResetListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            frame.reset();
        }
    }

    class MediiListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }
}
