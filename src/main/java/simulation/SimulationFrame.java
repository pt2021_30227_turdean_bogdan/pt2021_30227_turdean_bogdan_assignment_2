package simulation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class SimulationFrame extends JFrame {
    private JTextField noClienti = new JTextField(100);
    private JTextField minProcessing = new JTextField(100);
    private JTextField maxProcessing = new JTextField(100);
    private JTextField noServers = new JTextField(100);
    private JTextField minArrival = new JTextField(100);
    private JTextField maxArrival = new JTextField(100);
    private JButton start = new JButton("START");
    private JButton medii = new JButton("Medii");
    private JTextArea areaOut = new JTextArea(100, 100);
    private JTextField peakHour = new JTextField(100);
    private JTextField avgWaitingTime = new JTextField(100);
    private JTextField avgServiceTime = new JTextField(100);
    private JButton reset = new JButton("RESET");
    private JTextField timeLimit = new JTextField(100);
    private JScrollPane scroll = new JScrollPane(areaOut);

    public SimulationFrame(){
        JPanel panelTitlu = new JPanel();
        panelTitlu.setLayout(new FlowLayout());
        panelTitlu.add(new JLabel("Tema 2: Simulator cozi"));

        JPanel panelInput1 = new JPanel();
        panelInput1.setLayout(new BoxLayout(panelInput1, BoxLayout.X_AXIS));
        panelInput1.add(new JLabel("Numar clienti        "));
        panelInput1.add(noClienti);
        panelInput1.add(new JLabel("Minim procesare"));
        panelInput1.add(minProcessing);

        JPanel panelInput2 = new JPanel();
        panelInput2.setLayout(new BoxLayout(panelInput2, BoxLayout.X_AXIS));
        panelInput2.add(new JLabel("Maxim procesare"));
        panelInput2.add(maxProcessing);
        panelInput2.add(new JLabel("Numar servere   "));
        panelInput2.add(noServers);

        JPanel panelInput3 = new JPanel();
        panelInput3.setLayout(new BoxLayout(panelInput3, BoxLayout.X_AXIS));
        panelInput3.add(new JLabel("Minim arrival         "));
        panelInput3.add(minArrival);
        panelInput3.add(new JLabel("Maxim arrival      "));
        panelInput3.add(maxArrival);

        JPanel panelInput4 = new JPanel();
        panelInput4.setLayout(new BoxLayout(panelInput4, BoxLayout.X_AXIS));
        panelInput4.add(start);
        panelInput4.add(medii);
        panelInput4.add(reset);

        JPanel panelInput5 = new JPanel();
        panelInput5.setLayout(new BoxLayout(panelInput5, BoxLayout.X_AXIS));
        panelInput5.add(new JLabel("Timp simulare"));
        panelInput5.add(timeLimit);

        JPanel panelOutput1 = new JPanel();
        panelOutput1.setLayout(new FlowLayout());
        initArea();
        scroll.setVisible(true);
        areaOut.setEditable(false);
        panelOutput1.add(scroll);

        JPanel panelOutput2 = new JPanel();
        panelOutput2.setLayout(new BoxLayout(panelOutput2, BoxLayout.X_AXIS));
        panelOutput2.add(new JLabel("Peak hour            "));
        peakHour.setEditable(false);
        panelOutput2.add(peakHour);

        JPanel panelOutput3 = new JPanel();
        panelOutput3.setLayout(new BoxLayout(panelOutput3, BoxLayout.X_AXIS));
        panelOutput3.add(new JLabel("Avg waiting time"));
        avgWaitingTime.setEditable(false);
        panelOutput3.add(avgWaitingTime);

        JPanel panelOutput4 = new JPanel();
        panelOutput4.setLayout(new BoxLayout(panelOutput4, BoxLayout.X_AXIS));
        panelOutput4.add(new JLabel("Avg service time"));
        avgServiceTime.setEditable(false);
        panelOutput4.add(avgServiceTime);

        JPanel panelFinal = new JPanel();
        panelFinal.setLayout(new BoxLayout(panelFinal, BoxLayout.Y_AXIS));
        panelFinal.add(panelTitlu);
        panelFinal.add(panelInput1);
        panelFinal.add(panelInput2);
        panelFinal.add(panelInput3);
        panelFinal.add(panelInput5);
        panelFinal.add(panelInput4);
        panelFinal.add(panelOutput1);
        panelFinal.add(panelOutput2);
        panelFinal.add(panelOutput3);
        panelFinal.add(panelOutput4);

        this.setContentPane(panelFinal);
        this.pack();
        this.setTitle("Tema 2");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void startActionListener(ActionListener mal){
        start.addActionListener(mal);
    }

    public void resetActionListener(ActionListener mal){
        reset.addActionListener(mal);
    }

    public void mediiActionListener(ActionListener mal){
        medii.addActionListener(mal);
    }

    public void reset(){
        this.noClienti.setText("0");
        this.noServers.setText("0");
        this.minProcessing.setText("0");
        this.maxProcessing.setText("0");
        this.minArrival.setText("0");
        this.maxArrival.setText("0");
        this.areaOut.setText("");
        this.peakHour.setText("0");
        this.avgServiceTime.setText("0");
        this.avgWaitingTime.setText("0");
        this.timeLimit.setText("0");
        scroll.repaint();
        this.areaOut.repaint();
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setPreferredSize(new Dimension(1700, 800));
        scroll.setMinimumSize(new Dimension(0, 0));
    }

    public int getNoClienti(){
        return Integer.parseInt(noClienti.getText());
    }

    public int getNoServers(){
        return Integer.parseInt(noServers.getText());
    }

    public int getMinProcessing(){
        return Integer.parseInt(minProcessing.getText());
    }

    public int getMaxProcessing(){
        return Integer.parseInt(maxProcessing.getText());
    }

    public int getMinArrival(){
        return Integer.parseInt(minArrival.getText());
    }

    public int getMaxArrival(){
        return Integer.parseInt(maxArrival.getText());
    }

    public int getPeakHour(){
        return Integer.parseInt(peakHour.getText());
    }

    public int getAvgWaitingTime(){
        return Integer.parseInt(avgWaitingTime.getText());
    }

    public int getAvgServiceTime(){
        return Integer.parseInt(avgServiceTime.getText());
    }

    public int getTimeLimit(){
        return Integer.parseInt(timeLimit.getText());
    }

    public void setArea(String s){
        this.areaOut.append(s);
    }

    public void initArea(){
        scroll.repaint();
        this.areaOut.repaint();
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setPreferredSize(new Dimension(1700, 800));
        scroll.setMinimumSize(new Dimension(0, 0));
    }
}
