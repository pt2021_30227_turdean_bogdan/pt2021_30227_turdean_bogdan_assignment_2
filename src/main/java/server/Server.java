package server;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;

    public Server() {
        this.tasks = new ArrayBlockingQueue<Task>(100);
        this.waitingPeriod = new AtomicInteger(0);
    }

    public BlockingQueue<Task> getTasks() {
        return tasks;
    }

    public void setTasks(BlockingQueue<Task> tasks) {
        this.tasks = tasks;
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public void addTask(Task newTask){
        this.tasks.add(newTask);
        this.waitingPeriod.addAndGet(newTask.getProcessingPeriod());
    }

    public synchronized void run() {
        while(this.waitingPeriod.intValue() != 0){
            while(tasks.isEmpty()!=true){
                if(this.tasks.element()!=null) {
                    //this.tasks.element().setProcessingPeriod(this.tasks.element().getProcessingPeriod() - 1);
                    if(this.waitingPeriod.intValue() != 0){
                        try {
                            Thread.sleep(1000 * this.tasks.element().getProcessingPeriod());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    //tasks.remove();
                    this.waitingPeriod.addAndGet(-this.tasks.element().getProcessingPeriod());
                }
            }

        }
    }

    @Override
    public String toString() {
        String result="";
        int ok = 0;
        for(Task t: this.tasks){
            result = result + t.toString();
            ok = 1;
        }
        if(ok == 0)
            result = "closed";
        return result;
    }
}
