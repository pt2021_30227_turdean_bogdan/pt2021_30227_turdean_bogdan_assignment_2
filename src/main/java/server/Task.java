package server;

public class Task implements Comparable{
    private int id;
    private int arrivalTime;
    private int processingPeriod;
    private int finishTime;

    public Task(int id, int arrivalTime, int processingPeriod) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.processingPeriod = processingPeriod;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getProcessingPeriod() {
        return processingPeriod;
    }

    public void setProcessingPeriod(int processingPeriod) {
        this.processingPeriod = processingPeriod;
    }

    public int getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(int waitingPeriodOnChosenServer) {
        this.finishTime = this.arrivalTime + this.processingPeriod + waitingPeriodOnChosenServer;
    }

    @Override
    public int compareTo(Object o) {
        return this.arrivalTime - ((Task)o).getArrivalTime();
    }

    @Override
    public String toString() {
        return "(" + this.id + ", " + this.arrivalTime + ", " + this.processingPeriod + ")";
    }
}
